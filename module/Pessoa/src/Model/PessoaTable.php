<?php

namespace Pessoa\Model;

use Zend\Db\TableGateway\TableGatewayInterface;
use RunTimeException;

class PessoaTable {
    
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway){
        $this->tableGateway = $tableGateway;
    }

    public function getAll(){
        return $this->tableGateway->select();
    }

    public function getPessoa($id){
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if(!row){
            throw new RunTimeException(sprintf('Não foi encontrado o id', $id));
        }
        return $row;
    }

    public function salvarPessoa(Pessoa $pessoa){
        $data =[
            'id'=> $pessoa->getId(),
            'nome'=> $pessoa->getNome(),
            'sobrenome'=> $pessoa->getSobrenome(),
            'email'=> $pessoa->getEmail(),
            'situacao'=> $pessoa->getSituacao(),

        ];

        if($id === 0){
            $this->tableGateway->insert($data);
            return;
        }
        $this->tableGateway->update($data, ['id'=>$id]);
    }

    public function deletarPessoa($id){
        $this->tableGateway->delete(['id'=>(int)$id]);
    }
}