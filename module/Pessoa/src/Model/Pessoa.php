<?php

namespace Pessoa\Model;

class Pessoa {
    private $id;
    private $nome;
    private $sobrenome;
    private $email;
    private $situacao;

    public function exchangeArray(array $data){
        $this->id =!empty($data['id']) ? $data['id'] : null;
        $this->nome =!empty($data['nome']) ? $data['nome'] : null;
        $this->sobrenome =!empty($data['sobrenome']) ? $data['sobrenome'] : null;
        $this->email =!empty($data['email']) ? $data['email'] : null;
        $this->situacao =!empty($data['situacao']) ? $data['situacao'] : null;

        function getId(){
            return $this->id;
        }

        function setId($id){
            $this->id = $id;
        }

        function getNome(){
            return $this->nome;
        }

        function setNome($nome){
            $this->nome = $nome;
        }

        function getSobrenome(){
            return $this->sobrenome;
        }

        function setSobrenome($sobrenome){
            $this->sobrenome = $sobrenome;
        }

        function getEmail(){
            return $this->email;
        }

        function setEmail($email){
            $this->email = $email;
        }

        function getsituacao(){
            return $this->situacao;
        }

        function setSituacao($situacao){
            $this->situacao = $situacao;
        }
    }
}